'use strict';
(function() {
	function inheritense (parent, child) {
		var tempChild = child.prototype;	
		if (Object.create) {
			child.prototype = Object.create(parent.prototype);
		} else {
			function F() {};
			F.prototype = parent.prototype;
			child.prototype = new F();
		}

		for(var key in tempChild) {
			if (tempChild.hasOwnProperty(key)){
				child.prototype[key] = tempChild[key];	
			}	
		}
	};
	function Filter (form) {
		this.DOMElements = {
			tabCars: document.querySelector('#tabCars'),
			carsDiv: document.querySelector('#cars'),
			tabWatches: document.querySelector('#tabWatches'),
			watchesDiv: document.querySelector('#watches')
		};
		for (var key in form) {
			this.DOMElements[key] = form[key];
		};
	}
	Filter.prototype = {
		showHide: function(event) {
			if(event.currentTarget.classList != 'active') {
				event.target.classList.add('active');
				this.DOMElements.tabCars.classList.toggle('active');
				this.DOMElements.tabWatches.classList.toggle('active');
				this.DOMElements.carsDiv.classList.toggle('closed');
				this.DOMElements.watchesDiv.classList.toggle('closed');
			}
		},
		getPriceFilter: function() {
			return this.DOMElements.price.value;
		},
		getDateFilter: function() {
			return this.DOMElements.year.value;
		},
		showResult: function(result) {
			this.DOMElements.resultArea.innerHTML = result;
		}
	}
	
	//---------------------------------- фильтр CLOCK
	
	function ClockFilter (form, elements) {
		Filter.call(this, form);
		this.DOMEl = elements;
	}

	ClockFilter.prototype = {
		getMechanismFilter: function() {
			var id = this.DOMEl.mechanism.selectedIndex;
			return document.getElementsByName('mechanism')[id].value;
		},
		getGenderFilter: function() {
			var gender = this.DOMEl.gender
			var id = this.DOMEl.gender.length;
			for (var i = 0; i < id; i++){
				if (gender[i].checked){
					return gender[i].value;
				}
			}

		},
		createData: function(event) {
			event.preventDefault();
			this.clockFilterData = {
				price: this.getPriceFilter(),
				year: this.getDateFilter(),
				mechanism: this.getMechanismFilter(),
				gender:  this.getGenderFilter()
			};
			this.clockFilterDataJSON = JSON.stringify(this.clockFilterData);
			this.showResult(this.clockFilterDataJSON);
			console.log(this.clockFilterData);
		},
		initListeners: function() {
			this.DOMEl.submitBtn.addEventListener('click', this.createData.bind(this));
			this.DOMElements.tabWatches.addEventListener('click', this.showHide.bind(this));
		}
	}
	inheritense(Filter, ClockFilter);

	var clockFilter = new ClockFilter ({
		price: document.querySelector('#price'),
		year: document.querySelector('#year'),
		resultArea: document.querySelector('#resultWatches')
	},{
		gender: document.getElementsByName('gender'),
		mechanism: document.querySelector('#mechanism'),
		resultArea: document.querySelector('#resultWatches'),
		submitBtn: document.querySelector('#submitWatches')
	});
	clockFilter.initListeners();
	
	//------------------------------  фильтр CAR
	
	function CarFilter (form, elements){
		Filter.call(this, form);
		this.DOMEl = elements;
		this.statusCar = [];
	}

	CarFilter.prototype = Object.create(Filter.prototype);

	CarFilter.prototype.createData = function(event) {
		event.preventDefault();
		this.carFilterData = {
			price: this.getPriceFilter(),
			year: this.getDateFilter(),
			transmission: this.getRadioFilters(this.DOMEl.transmission),
			carBody: this.getRadioFilters(this.DOMEl.carBody),
			status: this.getStatusFilter()
		};
		this.carFilterDataJSON = JSON.stringify(this.carFilterData);
		this.showResult(this.carFilterDataJSON);
		console.log(this.carFilterData);
	}

	CarFilter.prototype.getRadioFilters = function(filter) {
		var element = filter;
		var id = filter.length;
		for (var i = 0; i < id; i++){
			if (element[i].checked){
				return element[i].value;
			}
		}
	}

	CarFilter.prototype.getStatusFilter = function() {
		var status = this.DOMEl.status;
		var id = status.length;
		var statusArray = [];
		for (var i = 0; i < id; i++){
			if (status[i].checked){
				statusArray.push(status[i].value);
			}
		}
		if (statusArray == 0){
			delete this.statusCar;
		}
		else {
			return this.statusCar = statusArray;
		}
	}

	CarFilter.prototype.initListeners = function(){
		this.DOMEl.submitBtn.addEventListener('click', this.createData.bind(this));
		this.DOMElements.tabCars.addEventListener('click', this.showHide.bind(this));
	}
	var carFilter = new CarFilter ({
		price: document.querySelector('#price'),
		year: document.querySelector('#year'),
		resultArea: document.querySelector('#resultCar')
	},{
		transmission: document.getElementsByName('transmission'),
		carBody: document.getElementsByName('carBody'),
		submitBtn: document.getElementById('submitCar'),
		status: document.getElementsByName('status')
	});
	carFilter.initListeners();
})();
